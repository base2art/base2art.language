namespace Base2art.Language.Extractor.Dom
{
    public class TextNode : Node
    {
        private readonly string value;
        private readonly NodeType nodeType;

        public TextNode(string value, NodeType nodeType)
        {
            this.value = value;
            this.nodeType = nodeType;
        }

        public override string Value =>  this.value;
        public override NodeType Type => this.nodeType;
    }
}