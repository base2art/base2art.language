namespace Base2art.Language.Extractor.Dom
{
    public class ApostropheNode : Node
    {
        public override NodeType Type => NodeType.Apostrophe;

        public override string Value { get; } =  "'";
    }
}