namespace Base2art.Language.Extractor.Dom
{
    public enum NodeType
    {
        Number,
        Dash,
        Period,
        Text,
        WhiteSpace,
        EndOfFile,
        Apostrophe,
        Punctuation
    }
}