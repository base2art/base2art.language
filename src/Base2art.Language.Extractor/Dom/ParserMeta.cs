namespace Base2art.Language.Extractor.Dom
{
    public class ParserMeta
    {
        public Node[] Nodes { get; set; }
        public bool? LikelyName { get; set; }
        public string Content { get; set; }
        public string[] ContentParts { get; set; }
    }
}