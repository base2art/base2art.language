namespace Base2art.Language.Extractor.Dom
{
    public class AnalyzerMetaItem
    {
        public Node[] Nodes { get; set; }
        public string Content { get; set; }
        public string[] ContentParts { get; set; }

        public int LikelyName { get; set; }
    }
}