namespace Base2art.Language.Extractor.Dom
{
    public class WhiteSpaceNode : Node
    {
        public override string Value { get; } =  " ";
        public override NodeType Type => NodeType.WhiteSpace;
    }
}