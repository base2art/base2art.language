namespace Base2art.Language.Extractor.Dom
{
    internal class ExpandedNode2 : Node
    {
        private readonly string shortcut;
        private readonly Node last;
        private readonly Node node;

        public ExpandedNode2(string shortcut, Node last, Node node)
        {
            this.shortcut = shortcut;
            this.last = last;
            this.node = node;
        }

        public override string Value  =>  this.shortcut;

        public override NodeType Type => NodeType.Text;
    }

    internal class ExpandedNode3 : Node
    {
        private readonly string shortcut;
        private readonly Node last;
        private readonly Node node;
        private readonly Node next;

        public ExpandedNode3(string shortcut, Node last, Node node, Node next)
        {
            this.shortcut = shortcut;
            this.last = last;
            this.node = node;
            this.next = next;
        }

        public override string Value  =>  this.shortcut;

        public override NodeType Type => NodeType.Text;
    }
}