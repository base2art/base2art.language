namespace Base2art.Language.Extractor.Dom
{
    public class DashNode : Node
    {
        public override string Value { get; } =  "-";
        public override NodeType Type => NodeType.Dash;
    }
}