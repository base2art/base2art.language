namespace Base2art.Language.Extractor.Dom
{
    public class PunctuationNode : Node
    {
        private readonly string currentValue;

        public PunctuationNode(string currentValue)
        {
            this.currentValue = currentValue;
        }

        public override NodeType Type => NodeType.Punctuation;

        public override string Value  =>  this.currentValue;
    }
}