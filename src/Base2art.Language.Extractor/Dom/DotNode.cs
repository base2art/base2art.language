namespace Base2art.Language.Extractor.Dom
{
    public class DotNode : Node
    {
        public override string Value { get; } = ".";
        public override NodeType Type => NodeType.Period;
    }
}