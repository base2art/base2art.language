namespace Base2art.Language.Extractor.Dom
{
    using System;
    using Base2art.Language;

    public abstract class Node : INode<NodeType>, IComparable<Node>
    {
        public abstract NodeType Type { get; }
        public abstract string Value { get; }

        public int CompareTo(Node other)
        {
            if (other == null)
            {
                return -1;
            }

            var otherType = (int) (other.Type);
            var thisType = (int) (this.Type);
            if (otherType != thisType)
            {
                return thisType.CompareTo(otherType);
            }

            var otherValue = other.Value;
            var thisValue = this.Value;
            if (otherValue != thisValue)
            {
                return String.Compare(thisValue, otherValue, StringComparison.OrdinalIgnoreCase);
            }

            return 0;
        }

        public override string ToString() => this.Value;
    }
}