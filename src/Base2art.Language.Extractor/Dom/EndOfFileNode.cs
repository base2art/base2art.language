namespace Base2art.Language.Extractor.Dom
{
    public class EndOfFileNode : Node
    {
        public override NodeType Type => NodeType.EndOfFile;
        public override string Value { get; } = "";
    }
}