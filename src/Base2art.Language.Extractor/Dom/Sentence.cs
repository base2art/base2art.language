namespace Base2art.Language.Extractor.Dom
{
    using System;
    using Base2art.Language;

    public class Sentence : TokenContainerBase<Node>
    {
        public TextNode CreateTextNode(string value, BodyTokenType tokenType)
        {
            return new TextNode(value, tokenType == BodyTokenType.Alpha ? NodeType.Text : NodeType.Number);
        }

        public Node CreateWhiteSpace()
        {
            return new WhiteSpaceNode();
        }

        public Node CreateDash()
        {
            return new DashNode();
        }

        public Node CreateApostrophe()
        {
            return new ApostropheNode();
        }

        public Node CreateDot()
        {
            return new DotNode();
        }

        public Node CreatePunctuation(IToken<BodyTokenType> current)
        {
            return new PunctuationNode(current.Value);
        }

        protected override Node EndOfFile() => new EndOfFileNode();
    }
}