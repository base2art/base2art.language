namespace Base2art.Language.Extractor
{
    using Base2art.Language;
    using Dom;

    public class SentenceMacroProcessor : MacroProcessor<Sentence, Node, NodeType>
    {
        public SentenceMacroProcessor() : base(new LanguageSettings(), new MacroContainer())
        {
        }

        private class MacroContainer : MacroContainer<Node, NodeType>
        {
            public MacroContainer()
            {
                this.Add(
                         NodeType.Text,
                         "w",
                         NodeType.Punctuation,
                         "/",
                         (x, y) => new ExpandedNode2("with", x, y));
                
                this.Add(
                         NodeType.Text,
                         "ft",
                         NodeType.Period,
                         ".",
                         (x, y) => new ExpandedNode2("featuring", x, y));
                
                this.Add(
                         NodeType.Text,
                         "ep",
                         NodeType.Period,
                         ".",
                         (x, y) => new ExpandedNode2("featuring", x, y));

                this.Add(
                         NodeType.Text,
                         null,
                         NodeType.Apostrophe,
                         null,
                         NodeType.Text,
                         "s",
                         (x, y, z) => null);
                
                this.Add(
                         NodeType.Text,
                         null,
                         NodeType.Apostrophe,
                         null,
                         NodeType.Text,
                         null,
                         (x, y, z) => new ExpandedNode3($"{x}{y}{z}", x, y, z));
            }
        }
    }
}