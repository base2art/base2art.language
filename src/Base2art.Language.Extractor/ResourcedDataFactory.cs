namespace Base2art.Language.Extractor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;

    public class ResourcedDataFactory : FileBasedDataFactory
    {
        protected override Task<IEnumerable<string>> SurnameLines()
            => Task.FromResult(ReadResourceLines("Base2art.Language.Extractor.Resx.surnames.lines"));

        protected override Task<IEnumerable<string>> GivenNameLines()
            => Task.FromResult(ReadResourceLines("Base2art.Language.Extractor.Resx.known-names.lines"));

        protected override Task<IEnumerable<string>> GetCommonWordsLines()
            => Task.FromResult(ReadResourceLines("Base2art.Language.Extractor.Resx.words.en"));

        private IEnumerable<string> ReadResourceLines(string resxName)
        {
            var asm = Assembly.GetExecutingAssembly();
            using (var stream = asm.GetManifestResourceStream(resxName))
            {
                using (var sr = new StreamReader(stream))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        yield return line;
                    }
                }
            }
        }

        /*
        public static async Task<Dictionary<string, int>> GivenNames()
        {
            var lines = await File.ReadAllLinesAsync("/home/tyoung/code/b2a/base2art.language/src/Base2art.Language/Resx/known-names.lines");
            Dictionary<string, int> words = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            foreach (var line in lines.Select(x => x.Trim()).Where(x => !String.IsNullOrWhiteSpace(x)))
            {
                var parts = line.Split(':');
                if (parts.Length == 2)
                {
                    words.Add(parts[0], Int32.Parse(parts[1]));
                }
            }

            return words;
        }

        public static async Task<Dictionary<string, int>> Surnames()
        {
            var lines = await File.ReadAllLinesAsync("/home/tyoung/code/b2a/base2art.language/src/Base2art.Language/Resx/surnames.lines");
            Dictionary<string, int> words = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            foreach (var line in lines.Select(x => x.Trim()).Where(x => !String.IsNullOrWhiteSpace(x)))
            {
                var parts = line.Split(',');
                if (parts.Length == 15)
                {
                    var value = Int32.Parse(parts[1]);

                    var cost = 1;
                    if (value < 1666)
                    {
                        cost = 1;
                    }
                    else if (value < 3333)
                    {
                        cost = 2;
                    }
                    else
                    {
                        cost = 3;
                    }

                    words.Add(parts[0], cost);
                }
            }

            return words;
        }
        */
    }
}