namespace Base2art.Language.Extractor
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Base2art.Language;
    using Base2art.Language.Internals;
    using Dom;

    public class SentenceLexer : LexerBase<BodyTokenizer, BodyTokenType, Sentence>
    {
        public SentenceLexer(Stream stream) : base(new BodyTokenizer(new StreamAdvancer(stream)))
        {
        }

        protected override void Start(Sentence doc, IEnumerator<IToken<BodyTokenType>> results)
        {
            this.SwallowSpace(results);
            var next = this.Word(doc, results);
            while (next)
            {
                next = this.Word(doc, results);
            }
        }

        private void SwallowSpace(IEnumerator<IToken<BodyTokenType>> tokenizer)
        {
            if (IsWhiteSpace(tokenizer.Current?.Type))
            {
                tokenizer.MoveNext();
                this.SwallowSpace(tokenizer);
            }
        }

        private void Reset(Sentence doc, IEnumerator<IToken<BodyTokenType>> tokenizer)
        {
            var next = this.Word(doc, tokenizer);
            while (next)
            {
                next = this.Word(doc, tokenizer);
            }
        }

        private bool Word(Sentence doc, IEnumerator<IToken<BodyTokenType>> tokenizer)
        {
            var currentToken = tokenizer.Current;

            if (currentToken?.Type == BodyTokenType.EOF)
            {
                return false;
            }

            if (currentToken?.Type == BodyTokenType.Alpha)
            {
                var process = this.Literal(doc, tokenizer, new StringBuilder(), BodyTokenType.Alpha);
                while (process.HasValue)
                {
                    var (next, sb) = process.Value;
                    process = this.Literal(doc, tokenizer, sb, next);
                }

                return false;
            }

            if (currentToken?.Type == BodyTokenType.Numeric)
            {
                var process =  this.Literal(doc, tokenizer, new StringBuilder(), BodyTokenType.Numeric);
                while (process.HasValue)
                {
                    var (next, sb) = process.Value;
                    process = this.Literal(doc, tokenizer, sb, next);
                }

                return false;
            }

            if (currentToken?.Type == BodyTokenType.WhiteSpace)
            {
                tokenizer.MoveNext();
                doc.Push(doc.CreateWhiteSpace());
                return true;
            }

            if (currentToken?.Type == BodyTokenType.OtherPunctuation)
            {
                doc.Push(doc.CreatePunctuation(tokenizer.Current));
                tokenizer.MoveNext();

                return true;
            }

            if (currentToken?.Type == BodyTokenType.Apostrophe)
            {
                doc.Push(doc.CreateApostrophe());
                tokenizer.MoveNext();

                // this.Word(doc, tokenizer);
                return true;
            }

            if (currentToken?.Type == BodyTokenType.Dot)
            {
                doc.Push(doc.CreateDot());
                tokenizer.MoveNext();

                // this.Word(doc, tokenizer);
                return true;
            }

            if (currentToken?.Type == BodyTokenType.Dash)
            {
                doc.Push(doc.CreateDash());
                tokenizer.MoveNext();

                // this.Word(doc, tokenizer);
                return true;
            }

            throw new IndexOutOfRangeException($"Item Not Parsible: '{currentToken?.Type}': {currentToken?.Value}");
        }

        private (BodyTokenType, StringBuilder)? Literal(Sentence doc, IEnumerator<IToken<BodyTokenType>> tokenizer, StringBuilder stringBuilder,
                                                        BodyTokenType tokenType)
        {
            var currentToken = tokenizer.Current;
            if (currentToken?.Type == BodyTokenType.WhiteSpace)
            {
                doc.Push(doc.CreateTextNode(stringBuilder.ToString(), tokenType));
                doc.Push(doc.CreateWhiteSpace());
                tokenizer.MoveNext();

                this.Reset(doc, tokenizer);
                return null;
            }

            if (currentToken?.Type == BodyTokenType.Dash)
            {
                doc.Push(doc.CreateTextNode(stringBuilder.ToString(), tokenType));
                doc.Push(doc.CreateDash());
                tokenizer.MoveNext();

                this.Reset(doc, tokenizer);
                return null;
            }

            if (currentToken?.Type == BodyTokenType.Dot)
            {
                doc.Push(doc.CreateTextNode(stringBuilder.ToString(), tokenType));
                doc.Push(doc.CreateDot());
                tokenizer.MoveNext();

                this.Reset(doc, tokenizer);
                return null;
            }

//            if (currentToken?.Type == BodyTokenType.Dash)
//            {
//                doc.Add(doc.CreateTextNode(stringBuilder.ToString(), tokenType));
//                doc.Add(doc.CreateDot());
//                tokenizer.MoveNext();
//
//                this.Reset(doc, tokenizer);
//                return;
//            }

            if (currentToken?.Type == BodyTokenType.Alpha)
            {
                if (tokenType == BodyTokenType.Numeric)
                {
                    stringBuilder.Append(currentToken.Value);
                    tokenizer.MoveNext();
                    // this.Literal(doc, tokenizer, stringBuilder, BodyTokenType.AlphaAndNumeric);
                    return (BodyTokenType.AlphaAndNumeric, stringBuilder);
                }

                stringBuilder.Append(currentToken.Value);
                tokenizer.MoveNext();
                // this.Literal(doc, tokenizer, stringBuilder, tokenType);
                return (tokenType, stringBuilder);
            }

            if (currentToken?.Type == BodyTokenType.Numeric)
            {
                if (tokenType == BodyTokenType.Alpha)
                {
                    stringBuilder.Append(currentToken.Value);
                    tokenizer.MoveNext();
                    // this.Literal(doc, tokenizer, stringBuilder, BodyTokenType.AlphaAndNumeric);
                    return (BodyTokenType.AlphaAndNumeric, stringBuilder);
                }

                stringBuilder.Append(currentToken.Value);
                tokenizer.MoveNext();
                // this.Literal(doc, tokenizer, stringBuilder, tokenType);
                return (tokenType, stringBuilder);
            }

            if (currentToken?.Type == BodyTokenType.EOF)
            {
                doc.Push(doc.CreateTextNode(stringBuilder.ToString(), tokenType));
                return null;
            }

            if (currentToken?.Type == BodyTokenType.Apostrophe)
            {
                doc.Push(doc.CreateTextNode(stringBuilder.ToString(), tokenType));
                doc.Push(doc.CreateApostrophe());
                tokenizer.MoveNext();

                this.Reset(doc, tokenizer);
                return null;
            }

            if (currentToken?.Type == BodyTokenType.OtherPunctuation)
            {
                doc.Push(doc.CreateTextNode(stringBuilder.ToString(), tokenType));
                doc.Push(doc.CreatePunctuation(tokenizer.Current));
                tokenizer.MoveNext();

                this.Reset(doc, tokenizer);
                return null;
            }

//            var currentValue = currentToken?.Value;
//            stringBuilder.Append(currentValue);
//            if (!tokenizer.MoveNext())
//            {
//                doc.Add(doc.CreateTextNode(stringBuilder.ToString()));
//                return;
//            }
//            this.Literal(doc, tokenizer, stringBuilder);

            throw new IndexOutOfRangeException($"Item Not Parsible: '{currentToken?.Type}': {currentToken?.Value}");
        }

        protected bool IsWhiteSpace(BodyTokenType? type)
        {
            return type == BodyTokenType.WhiteSpace;
        }
    }
}