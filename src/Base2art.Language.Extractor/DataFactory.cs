namespace Base2art.Language.Extractor
{
    using System.Threading.Tasks;

    public interface IDataFactory
    {
        Task<IDataContainer> LoadData();
    }
}