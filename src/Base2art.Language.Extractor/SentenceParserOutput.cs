namespace Base2art.Language.Extractor
{
    using System.Collections;
    using System.Collections.Generic;
    using Dom;

    public class SentenceParserOutput : IParserOutput<ParserMeta>
    {
        private readonly List<ParserMeta> items = new List<ParserMeta>();

        public void Add(ParserMeta parserMeta)
        {
            this.items.Add(parserMeta);
        }

        public IEnumerator<ParserMeta> GetEnumerator() => this.items.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}