namespace Base2art.Language.Extractor
{
    using System.Collections.Generic;
    using Dom;

    public interface IAnalyzerOutput
    {
        IEnumerable<KeyValuePair<string, AnalyzerMetaItem>> Unfold();

        IEnumerable<AnalyzerMetaParent> Roots();
    }

    public class AnalyzerMetaParent
    {
        private readonly List<AnalyzerMetaItem> children;

        public AnalyzerMetaParent(List<AnalyzerMetaItem> children)
        {
            this.children = children;
        }

        public Node[] Nodes { get; set; }
        public string Content { get; set; }
        public string[] ContentParts { get; set; }

        public AnalyzerMetaItem[] Children => this.children.ToArray();
    }
}