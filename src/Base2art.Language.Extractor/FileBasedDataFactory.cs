namespace Base2art.Language.Extractor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public abstract class FileBasedDataFactory : IDataFactory
    {
        async Task<IDataContainer> IDataFactory.LoadData()
        {
            return new DataContainer(
                                     await this.CompoundNamesParts(),
                                     await this.CommonWords(),
                                     await this.GivenNames(),
                                     await this.Surnames());
        }

        private async Task<IReadOnlyDictionary<string, int>> Surnames()
            => CleanLinesInParts(await this.SurnameLines(), ',', 15).ToDictionary(
                                                                                  line => line[0],
                                                                                  line => ComputeCost(int.Parse(line[1])),
                                                                                  StringComparer.OrdinalIgnoreCase);

        protected abstract Task<IEnumerable<string>> SurnameLines();

        private async Task<Dictionary<string, int>> CommonWords()
            => CleanLinesInParts(await this.GivenNameLines(), ':', 2).ToDictionary(
                                                                                   line => line[0],
                                                                                   line => int.Parse(line[1]),
                                                                                   StringComparer.OrdinalIgnoreCase);

        private async Task<Dictionary<string, int>> GivenNames()
            => CleanLinesInParts(await this.GivenNameLines(), ':', 2).ToDictionary(
                                                                                   line => line[0],
                                                                                   line => int.Parse(line[1]),
                                                                                   StringComparer.OrdinalIgnoreCase);

        protected abstract Task<IEnumerable<string>> GivenNameLines();

        protected abstract Task<IEnumerable<string>> GetCommonWordsLines();

        protected virtual async Task<HashSet<string>> CompoundNamesParts()
        {
            return new HashSet<string>
                   {
                       "los",
                       "le",
                       "al",
                       "do",
                       "du",
                       "den",
                       "del",
                       "dos",
                       "la",
                       "bin",
                       "van",
                       "von",
                       "der",
                       "de",
                       "ben",
                       "bint",

                       "da",
                       "dela",
                       "el"
                   };
        }

        private static int ComputeCost(int value)
            => value < 1666 ? 1 :
               value < 3333 ? 2 : 3;

        private static IEnumerable<string[]> CleanLinesInParts(IEnumerable<string> lines, char sep, int i)
            => CleanLines(lines).Select(line => line.Split(sep)).Where(parts => parts.Length == i);

        private static IEnumerable<string> CleanLines(IEnumerable<string> lines)
            => lines.Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x));

        private class DataContainer : IDataContainer
        {
            private readonly HashSet<string> compoundNamesParts;
            private readonly IReadOnlyDictionary<string, int> words;
            private readonly IReadOnlyDictionary<string, int> givenNames;
            private readonly IReadOnlyDictionary<string, int> surnames;

            public DataContainer(
                HashSet<string> compoundNamesParts,
                IReadOnlyDictionary<string, int> words,
                IReadOnlyDictionary<string, int> givenNames,
                IReadOnlyDictionary<string, int> surnames)
            {
                this.compoundNamesParts = compoundNamesParts;
                this.words = words;
                this.givenNames = givenNames;
                this.surnames = surnames;
            }

            public HashSet<string> CompoundNamesParts() => this.compoundNamesParts;

            public IReadOnlyDictionary<string, int> Words() => this.words;

            public IReadOnlyDictionary<string, int> GivenNames() => this.givenNames;

            public IReadOnlyDictionary<string, int> Surnames() => this.surnames;
        }
    }
}