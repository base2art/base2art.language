namespace Base2art.Language.Extractor
{
    using System.Collections.Generic;

    public interface IDataContainer
    {
        HashSet<string> CompoundNamesParts();
        IReadOnlyDictionary<string, int> Words();
        IReadOnlyDictionary<string, int> GivenNames();
        IReadOnlyDictionary<string, int> Surnames();
    }
}