namespace Base2art.Language.Extractor
{
    using System.Collections.Generic;
    using System.Linq;
    using Dom;

    public class AnalyzerPusher
    {
        private readonly List<AnalyzerMetaItem> analyzerMetaParent;

        public AnalyzerPusher(List<AnalyzerMetaItem> analyzerMetaParent)
        {
            this.analyzerMetaParent = analyzerMetaParent;
        }

        public AnalyzerMetaItem[] Children => this.analyzerMetaParent.ToArray();

//        public void Add(Node[] takeOf2, int getScore)
//        {
//        this.analyzerMetaParent.Add(new AnalyzerMetaItem
//                                    {
//                                        Content = 
//                                    });
//        }

        public AnalyzerMetaItem Push(Node[] parts, int score)
        {
            var contentParts = parts.Select(x => x.ToString()).ToArray();
            var analyzerMetaItem = new AnalyzerMetaItem
                                   {
                                       Content = string.Join("", contentParts),
                                       ContentParts = contentParts,
                                       Nodes = parts,
                                       LikelyName = score
                                   };
            this.analyzerMetaParent.Add(analyzerMetaItem);

            return analyzerMetaItem;
        }
    }
}