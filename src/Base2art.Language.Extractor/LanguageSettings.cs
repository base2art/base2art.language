namespace Base2art.Language.Extractor
{
    using Base2art.Language;
    using Dom;

    public class LanguageSettings : ILanguageSettings<NodeType>
    {
        public NodeType StopToken { get; } = NodeType.EndOfFile;
    }
}