namespace Base2art.Language.Extractor
{
    public enum WordType
    {
        StrongWord,
        Word,
        WeakWord,
        
        ProperName,
        WordOrName,
        Other,
        WordOrStrongName,
        WordOrWeakName,
        
        Joiner
    }
}