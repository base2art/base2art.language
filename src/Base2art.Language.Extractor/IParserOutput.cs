namespace Base2art.Language.Extractor
{
    using System;
    using System.Collections.Generic;
//    using System.Collections;
    using Base2art.Language;

    public interface IParserOutput<TParserMeta> : IEnumerable<TParserMeta>
    {
    }
}