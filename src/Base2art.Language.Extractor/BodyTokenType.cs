namespace Base2art.Language.Extractor
{
    public enum BodyTokenType
    {
        Ignore,

        Alpha,
        Numeric,

        AlphaAndNumeric,
//        MonkeyTail,

        EOF,

//        Asterisk,

        WhiteSpace,

//        LessThan,
//
//        GreaterThan,
//
//        OpenBrace,
//
//        CloseBrace,

        Dash,

//        Colon,

//        MethodBreak,
        //
        //
        //        Word,
        //
        //        Symbol,
        //
        //        Keyword,
        //
        //        Number,
        //
//        Pound,

        //
        Dot,

        //
        //
        //        Comma,
        //
        //        NewLine,
        //
        Apostrophe,
        OtherPunctuation
    }
}