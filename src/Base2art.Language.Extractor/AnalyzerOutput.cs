namespace Base2art.Language.Extractor
{
    using System.Collections.Generic;
    using System.Linq;
    using Dom;

    public class AnalyzerOutput : IAnalyzerOutput
    {
        private readonly int minScore;
        private readonly List<AnalyzerMetaParent> backing = new List<AnalyzerMetaParent>();

        public AnalyzerOutput(int minScore)
        {
            this.minScore = minScore;
        }

        public AnalyzerPusher Push(Node[] parts, int score)
        {
            var children = new List<AnalyzerMetaItem> { };
            var analyzerMetaParent = new AnalyzerMetaParent(children)
                                     {
                                     };
            this.backing.Add(analyzerMetaParent);

            var pusher = new AnalyzerPusher(children);

            var newItem = pusher.Push(parts, score);

            analyzerMetaParent.Content = newItem.Content;
            analyzerMetaParent.Nodes = newItem.Nodes;
            analyzerMetaParent.ContentParts = newItem.ContentParts;

            return pusher;
        }

//        private AnalyzerPusher Push(AnalyzerMetaItem analyzerMetaItem)
//        {
//
////            if (analyzerMetaItem.LikelyName >= minScore)
////            {
//////                this.backing.Add(probableNameKey, analyzerMetaItem);
////            }
//
//            //(probableNameKey, new AnalyzerMeta {Nodes = parts, LikelyName = 0});
//        }

        public IEnumerable<KeyValuePair<string, AnalyzerMetaItem>> Unfold()
        {
            return this.backing.SelectMany(x => x.Children)
                       .Select(x => new KeyValuePair<string, AnalyzerMetaItem>(x.Content, x))
                       .Where(x => x.Value.LikelyName >= this.minScore);
        }

        public IEnumerable<AnalyzerMetaParent> Roots()
        {
            return this.backing;
        }

//        public IEnumerable<KeyValuePair<string, AnalyzerMetaItem>> Unfold()
//        {
//            return this.backing;
//        }
    }
}