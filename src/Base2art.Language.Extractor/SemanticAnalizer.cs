namespace Base2art.Language.Extractor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Dom;

    public class SemanticAnalyzer
    {
        private readonly IDataContainer data;

        public SemanticAnalyzer(IDataContainer data)
        {
            this.data = data;
        }

        public IAnalyzerOutput ProbableNames(IParserOutput<ParserMeta> probableNames, int minScore)
        {
            var analyzerMetas = new AnalyzerOutput(minScore);

            foreach (var probableName in probableNames)
            {
                if (probableName.Nodes.Any(x => x.Type == NodeType.Text))
                {
                    this.AddItem(analyzerMetas, probableName.Content, probableName.Nodes, probableName.LikelyName);
                }
            }

            return analyzerMetas;
        }

        private void AddItem(
            AnalyzerOutput analyzerMetas,
            string probableNameKey,
            Node[] parts,
            bool? likelyNameWrapper)
        {
            var likelyName = 50;

            if (likelyNameWrapper.HasValue)
            {
                likelyName = likelyNameWrapper.Value ? 75 : 0;
            }

            var score = this.GetScore(parts, likelyName);

            BlowOut(analyzerMetas.Push(parts, score));
        }

        private void BlowOut(AnalyzerPusher push)
        {
            var root = push.Children.First();

            var rootNodes = root.Nodes;
            for (int i = 0; i < rootNodes.Length; i++)
            {
                if (rootNodes.Skip(i).FirstOrDefault()?.Type == NodeType.Text)
                {
                    var rootLikelyName = root.LikelyName;
                    if (rootLikelyName == 0)
                    {
                        rootLikelyName = 50;
                    }

                    {
                        var takeOf2 = this.Take(rootNodes.Skip(i).ToArray(), 2).ToArray();
                        if (takeOf2.Length > 0 && takeOf2.Length < rootNodes.Length)
                        {
                            push.Push(takeOf2, this.GetScore(takeOf2, rootLikelyName));
                        }
                    }

                    {
                        var takeOf3 = this.Take(rootNodes.Skip(i).ToArray(), 3).ToArray();
                        if (takeOf3.Length > 0 && takeOf3.Length < rootNodes.Length)
                        {
                            push.Push(takeOf3, this.GetScore(takeOf3, rootLikelyName));
                        }
                    }

                    {
                        var takeOf4 = this.Take(rootNodes.Skip(i).ToArray(), 4).ToArray();
                        if (takeOf4.Length > 0 && takeOf4.Length < rootNodes.Length)
                        {
                            push.Push(takeOf4, this.GetScore(takeOf4, rootLikelyName));
                        }
                    }
                }
            }
        }

        private Node[] Take(Node[] arr, int p1)
        {
            int counter = 0;

            List<Node> sub = new List<Node>();

            for (int i = 0; i < arr.Length; i++)
            {
                var x = arr[i];
                if (x.Type == NodeType.Text)
                {
                    counter += 1;
                }

                sub.Add(x);

                if (counter == p1)
                {
                    return sub.ToArray();
                }
            }

            return new Node[0];
        }

        private  int GetScore(IReadOnlyList<Node> parts, int likelyName)
        {
            if (parts.Count == 1)
            {
                var words = this.data.Words();
                if (words.ContainsKey(parts[0].ToString()))
                {
                    return 0;
                }
            }

            var nameTypes = new[] {NodeType.WhiteSpace, NodeType.Text, NodeType.Apostrophe, NodeType.Dash, NodeType.Period};

            if (parts.All(x => x.Type == NodeType.WhiteSpace || x.Type == NodeType.Text))
            {
                var types = parts.Where(x => x.Type == NodeType.Text).Select((x, i) => this.GetTypeOf(x, i, parts));
                return MapScore(types, likelyName);
                /*foreach (var part in parts.Where(x => x.Type == NodeType.Text))
                {
                    if (likelyName != 0)
                    {
                        var type = GetTypeOf(part);

                        var offset = this.MapScoreOffest(type, likelyName);

                        likelyName += offset;
                    }
                }*/
            }
            else if (parts.All(x => nameTypes.Contains(x.Type)))
            {
                var types = parts.Where(x => x.Type == NodeType.Text).Select((x, i) => this.GetTypeOf(x, i, parts));
                return MapScore(types, likelyName);
                /*foreach (var part in parts.Where(x => x.Type == NodeType.Text))
                {
                    if (likelyName != 0)
                    {
                        var type = GetTypeOf(part);

                        var offset = this.MapScoreOffest(type, likelyName);
                        likelyName += offset;
                    }
                }*/
            }

            return likelyName;
        }

        private int MapScore(IEnumerable<WordType> types, int likelyName)
        {
            if (likelyName == 0)
            {
                return 0;
            }

            var parts = types.ToArray();

            var pointParts = parts.Select(type => this.MapScoreOffest(type)).ToArray();

            if (pointParts.Any(x => x == int.MinValue))
            {
                return 0;
            }

            var points = pointParts.Sum();

            if (points == 0)
            {
                return likelyName;
            }

            if (points > 0)
            {
                var percentToPlayWith = 100 - likelyName;

                // 34 
                // points := 12
                // maxPoints := 16
                // 12 / 16 * 34 = 

                return likelyName + ((points * percentToPlayWith) / (5 * parts.Length));

                // more likely name
            }
            else
            {
                // more likely word

                // 34 

                // points := -12
                // maxPoints := 16
                // (-12 / 16) * 34 = 

                var percentToPlayWith = likelyName;
                return likelyName + ((points * percentToPlayWith) / (5 * parts.Length));
            }
        }

        private int MapScoreOffest(WordType type)
        {
//            var max = likelyName < 50 ? 50 : 100;
//            var max = 100;
            switch (type)
            {
                case WordType.Joiner:
                    return int.MinValue;

                case WordType.WeakWord:
                    return -1;
                case WordType.Word:
                    return -3;
                case WordType.StrongWord:
                    return -5;

                case WordType.ProperName:
                    return 5;

                case WordType.WordOrWeakName:
                    return 1;
                case WordType.WordOrName:
                    return 3;
                case WordType.WordOrStrongName:
                    return 5;

                case WordType.Other:
                    return 0;
            }

            throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
/*

        private int MapScorePoint(WordType type, int likelyName)
        {
//            var max = likelyName < 50 ? 50 : 100;
            var max = 100;
            switch (type)
            {
                case WordType.Joiner:
                    return 0 - (likelyName);

                case WordType.WeakWord:
                    return (int) (0 - (likelyName / 4));
                case WordType.Word:
                    return 0 - (likelyName / 2);
                case WordType.StrongWord:
                    return (int) (0 - (likelyName / 1.5));
                case WordType.ProperName:
                    return (max - likelyName) / 2;
                case WordType.WordOrWeakName:
                    return (max - likelyName) / 4;
                case WordType.WordOrName:
                    return (max - likelyName) / 2;
                case WordType.WordOrStrongName:
                    return (int) ((max - likelyName) / 1.5);
                case WordType.Other:
                    return 0;
            }

            throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }*/

        private WordType GetTypeOf(Node part, int i, IReadOnlyList<Node> parts)
        {
            var value = part.ToString();

            var joiners = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
                          {"in", "the", "an", "a", "with", "and", "or"};

            if (joiners.Contains(value))
            {
                return WordType.Joiner;
            }

            if (value.Length == 1)
            {
                return char.IsUpper(value[0]) ? WordType.ProperName : WordType.Joiner;
            }

            if (!this.data.CompoundNamesParts().Contains(value) && value == value.ToLowerInvariant())
            {
                return WordType.Joiner;
            }

            var surnames = this.data.Surnames();
            var words = this.data.Words();
            if (surnames.ContainsKey(value) && words.ContainsKey(value))
            {
                if (value == value.ToLowerInvariant())
                {
                    return WordType.StrongWord;
                }

                switch (surnames[value])
                {
                    case 3:
                        return WordType.WordOrWeakName;
                    case 2:
                        return WordType.WordOrName;
                    default:
                        return WordType.WordOrStrongName;
                }
            }

            var namesInTheDictionary = this.data.GivenNames();
            if (namesInTheDictionary.ContainsKey(value) && words.ContainsKey(value))
            {
                if (value == value.ToLowerInvariant())
                {
                    return WordType.StrongWord;
                }

                var valueFromCommonName = namesInTheDictionary[value];
                if (valueFromCommonName < 20)
                {
                    return WordType.WordOrWeakName;
                }

                if (valueFromCommonName < 50)
                {
                    return WordType.WordOrName;
                }

                return WordType.WordOrStrongName;
            }

            var compoundNameParts = this.data.CompoundNamesParts();
            if (words.ContainsKey(value))
            {
                if (compoundNameParts.Contains(value))
                {
                    return WordType.WordOrName;
                }

                if (value == value.ToLowerInvariant())
                {
                    return WordType.Joiner;
                }

                var wordScore = words[value];
                switch (wordScore)
                {
                    case 1:
                        return WordType.StrongWord;
                    case 2:
                        return WordType.Word;
                    default:
                        return WordType.WeakWord;
                }
            }

            if (compoundNameParts.Contains(value))
            {
                return WordType.WordOrName;
            }

            if (value == value.ToLowerInvariant())
            {
                return WordType.WeakWord;
            }

//            if (this.commonNamesInTheDictionary.ContainsKey(value))
            {
                return WordType.ProperName;
            }
        }
    }
}