namespace Base2art.Language.Extractor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Dom;

    public class SentenceParser
    {
//        private readonly HashSet<string> enWords;

//        public SentenceParser(HashSet<string> enWords)
//        {
////            this.enWords = enWords;
//        }

// Node, NodeType
        public IParserOutput<ParserMeta> ProbableNames(IEnumerable<Node> nodes)
        {
//            var names = new Dictionary<string[], ParserMeta>();
            var enumer = nodes.GetEnumerator();
            if (!enumer.MoveNext())
            {
                return new SentenceParserOutput();
            }

            var output = new SentenceParserOutput();
            this.Roll(enumer, new List<Node>(), output, null);

            return output; // names.ToDictionary(x => string.Join("", x.Key), x => x.Value);
        }

        public IParserOutput<ParserMeta> ProbableNames(Sentence sentence)
        {
            var nodes = sentence.Tokens();
            return ProbableNames(nodes);
        }

        private void Roll(IEnumerator<Node> nodes, List<Node> builder, SentenceParserOutput names, Node lastNode)
        {
            var node = nodes?.Current;
            var last = lastNode?.Type;

            if (node?.Type == NodeType.EndOfFile)
            {
                this.RollUpPrevious(builder, names, null);
                return;
            }

            if (node?.Type == NodeType.Number)
            {
                this.RollUpPrevious(builder, names, null);

                nodes.MoveNext();
                this.Roll(nodes, new List<Node>(), names, node);
                return;
            }

            if (node?.Type == NodeType.Text)
            {
                if (this.IsConjunction(node))
                {
                    this.RollUpPrevious(builder, names, null);
                    this.RollUpPrevious(new List<Node> {node}, names, false);
                    nodes.MoveNext();

                    this.Roll(nodes, new List<Node>(), names, node);
                    return;
                }

                builder.Add(node);
                nodes.MoveNext();
                this.Roll(nodes, builder, names, node);
                return;
            }

            if (node?.Type == NodeType.Period)
            {
                if (last == NodeType.Text && lastNode.ToString().Length > 1)
                {
                    this.RollUpPrevious(builder, names, null);
                    this.RollUpPrevious(new List<Node> {node}, names, false);
                    nodes.MoveNext();
                    this.Roll(nodes, new List<Node>(), names, node);
                    return;
                }

                builder.Add(node);

                nodes.MoveNext();
                this.Roll(nodes, builder, names, node);
                return;
            }

            if (node?.Type == NodeType.Punctuation)
            {
                if (last == NodeType.Text)
                {
                    if (nodes.MoveNext())
                    {
                        if (nodes?.Current?.Type == NodeType.Text)
                        {
                            builder.Add(node);

                            var nextItem = nodes?.Current;
                            builder.Add(nextItem);
                            nodes.MoveNext();

                            this.Roll(nodes, builder, names, nextItem);
                            return;
                        }

                        this.RollUpPrevious(builder, names, null);
                        this.RollUpPrevious(new List<Node> {node}, names, false);
                        this.Roll(nodes, new List<Node>(), names, node);
                        return;
                    }
                }

                this.RollUpPrevious(builder, names, null);
                this.RollUpPrevious(new List<Node> {node}, names, false);
                nodes.MoveNext();
                this.Roll(nodes, new List<Node>(), names, node);
                return;
            }

            if (node?.Type == NodeType.Apostrophe)
            {
                if (last == NodeType.Text && nodes.MoveNext())
                {
                    var afterApos = nodes?.Current;
                    if (afterApos?.Type == NodeType.Text)
                    {
                        if (string.Equals(afterApos?.ToString(), "s", StringComparison.OrdinalIgnoreCase))
                        {
                            this.RollUpPrevious(builder, names, true);
                            nodes.MoveNext();

                            this.Roll(nodes, new List<Node>(), names, afterApos);
                            return;
                        }
                        else
                        {
                            builder.Add(node);
                            builder.Add(afterApos);
                            nodes.MoveNext();

                            this.Roll(nodes, builder, names, afterApos);
                            return;
                        }
                    }

                    nodes.MoveNext();
                    this.Roll(nodes, builder, names, node);
                    return;
                }

                nodes.MoveNext();
                this.Roll(nodes, builder, names, node);
                return;
            }

            if (node?.Type == NodeType.WhiteSpace)
            {
                nodes.MoveNext();

                // SWALLOW
                if (nodes?.Current?.Type == NodeType.WhiteSpace)
                {
                    this.Roll(nodes, builder, names, node);
                    return;
                }

                builder.Add(node);
                this.Roll(nodes, builder, names, node);
                return;
            }

            if (node?.Type == NodeType.Dash)
            {
                if (last == NodeType.Text)
                {
                    if (nodes.MoveNext())
                    {
                        var nodesCurrent = nodes?.Current;
                        if (nodesCurrent?.Type == NodeType.Text)
                        {
                            builder.Add(node);
//                        builder.Add("-");
                            builder.Add(nodesCurrent);
                            nodes.MoveNext();

                            this.Roll(nodes, builder, names, nodesCurrent);
                            return;
                        }

                        this.Roll(nodes, builder, names, nodesCurrent);
                        return;
                    }
                }

                this.RollUpPrevious(builder, names, null);
                this.RollUpPrevious(new List<Node> {node}, names, false);
                nodes.MoveNext();
                this.Roll(nodes, new List<Node>(), names, node);
                return;
//                builder.Add(node);
//                nodes.MoveNext();
//                this.Roll(nodes, builder, names, NodeType.Text);
//                return;
            }

            throw new IndexOutOfRangeException($"Can I get here?: {node?.Type}");
        }

        private readonly HashSet<string> conjunctions = new HashSet<string> {"and", "with", "from"};

        private bool IsConjunction(Node node)
        {
            return this.conjunctions.Contains(node.ToString());
        }

        private void RollUpPrevious(List<Node> builder, SentenceParserOutput names, bool? likelyName)
        {
            var parts = builder.SkipWhile(x => x.Type == NodeType.WhiteSpace)
                               .Reverse()
                               .SkipWhile(x => x.Type == NodeType.WhiteSpace)
                               .Reverse()
                               .ToArray();

            if (parts.Length > 0)
            {
//                if (parts.Length == 1)
//                {
//                    if (this.enWords.Contains(parts[0].ToString()))
//                    {
//                        names.Add(parts.Select(x => x.ToString()).ToArray(), new ParserMeta
//                                                                             {
//                                                                                 LikelyName = false,
//                                                                                 Nodes = parts
//                                                                             });
//                        return;
//                    }
//                }

                names.Add(new ParserMeta
                          {
                              Content = string.Join("", parts.Select(x => x.ToString()).ToArray()),
                              ContentParts = parts.Select(x => x.ToString()).ToArray(),
                              LikelyName = likelyName,
                              Nodes = parts
                          });
            }
        }
    }
}