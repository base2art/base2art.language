namespace Base2art.Language.Extractor
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dom;

    public class DefaultNameExtractor
    {
        private readonly int minScore;
        private readonly SentenceMacroProcessor macroProcessor;
        private readonly Task<SemanticAnalyzer> loader;
        private readonly SentenceParser parser;

        public DefaultNameExtractor(int minScore)
        {
            this.minScore = minScore;

            this.macroProcessor = new SentenceMacroProcessor();
            // IDataFactory data = new HardCodedDataFactory();
            IDataFactory data = new ResourcedDataFactory();
            this.loader = this.CreateLoader(data);
            this.parser = new SentenceParser();
        }

        private async Task<SemanticAnalyzer> CreateLoader(IDataFactory factory)
        {
            return new SemanticAnalyzer(await factory.LoadData());
        }

        public async Task<IEnumerable<KeyValuePair<string, AnalyzerMetaItem>>> Search(string searchableText)
        {
            var lexer = new SentenceLexer(searchableText.AsStream());
            var sentence = new Sentence();
            lexer.Lex(sentence);
            sentence = this.macroProcessor.Run(sentence);
            var semanticAnalyzer = await this.loader;
            var names = semanticAnalyzer.ProbableNames(this.parser.ProbableNames(sentence), this.minScore);

            return names.Unfold();
        }
    }
}