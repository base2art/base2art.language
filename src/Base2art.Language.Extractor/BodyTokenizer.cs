﻿namespace Base2art.Language.Extractor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Base2art.Language;
    using Base2art.Language.Internals;

    public class BodyTokenizer : TokenizerBase<BodyTokenType>
    {
        protected override void CreateSingleCharTokensLookup(Dictionary<char, BodyTokenType> first, List<Func<char, BodyTokenType?>> fallback)
        {
            base.CreateSingleCharTokensLookup(first, fallback);

            // COMMENT
//            first.Add('*', BodyTokenType.Asterisk);

            // EXPR ESCAPED
//            first.Add('{', BodyTokenType.OpenBrace);
//            first.Add('}', BodyTokenType.CloseBrace);

            // EXPR RAW
//            first.Add('<', BodyTokenType.LessThan);
//            first.Add('>', BodyTokenType.GreaterThan);

            first.Add('\t', BodyTokenType.WhiteSpace);
            first.Add(' ', BodyTokenType.WhiteSpace);
            first.Add('\n', BodyTokenType.WhiteSpace);
            first.Add('\r', BodyTokenType.WhiteSpace);
            first.Add('\'', BodyTokenType.Apostrophe);

            // Function
            first.Add('-', BodyTokenType.Dash);
//            first.Add(':', BodyTokenType.OtherPunctuation);
//            first.Add('#', BodyTokenType.Pound);
            first.Add('.', BodyTokenType.Dot);

            fallback.Add((x) => char.IsLetter(x) ? BodyTokenType.Alpha : new BodyTokenType?());
            fallback.Add((x) => char.IsDigit(x) ? BodyTokenType.Numeric : new BodyTokenType?());
            fallback.Add((x) => BodyTokenType.OtherPunctuation);
//            char.IsPunctuation(x) || x == '©' ? : new BodyTokenType?()
        }

        public BodyTokenizer(StreamAdvancer stream)
            : base(stream)
        {
        }

        protected override BodyTokenType EndingToken
        {
            get { return BodyTokenType.EOF; }
        }

        protected override bool HandleMultiCharacterToken(LinkedList<Token<BodyTokenType>> tokens, StringBuilder sb, char c)
        {
            // NO MULTI CHARACTER TOKENS??
//            base.HandleMultiCharacterToken(tokens, sb, c);

            sb.Append(c);
            return this.Reset(tokens, sb, BodyTokenType.Ignore, true);
        }
    }
}