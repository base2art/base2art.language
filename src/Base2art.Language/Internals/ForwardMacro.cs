namespace Base2art.Language.Internals
{
    using System;

    internal class ForwardMacro<TToken> : IForwardMacro<TToken>
    {
//        private readonly Func<TNode, TNode, TNode, string> newTextFunc;

        public ForwardMacro(
            TToken backType, string backText,
            TToken currentType, string currentText,
            TToken forwardType, string forwardText)
//            Func<TNode, TNode, TNode, string> newText)
        {
            this.PreviousNodeType = backType;
            this.PreviousNodeValue = backText;
            this.CurrentNodeType = currentType;
            this.CurrentNodeValue = currentText;
            this.ForwardNodeType = forwardType;
            this.ForwardNodeValue = forwardText;
//            this.newTextFunc = newText;
        }

        public TToken PreviousNodeType { get; }
        public string PreviousNodeValue { get; }

        public TToken CurrentNodeType { get; }
        public string CurrentNodeValue { get; }

        public TToken ForwardNodeType { get; }
        public string ForwardNodeValue { get; }

//        public string NewText => this?.newTextFunc(default, default, default);
    }
}