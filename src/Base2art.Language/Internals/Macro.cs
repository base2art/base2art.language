namespace Base2art.Language.Internals
{
    using System;

    internal class Macro<TToken> : IMacro<TToken>
    {
//        private Func<TNode, TNode, string> newTextFunc;
//        Func<TNode, TNode, string> newText

        public Macro(TToken backType, string backText, TToken currentType, string currentText)
        {
            this.PreviousNodeType = backType;
            this.PreviousNodeValue = backText;
            this.CurrentNodeType = currentType;
            this.CurrentNodeValue = currentText;
//            this.newTextFunc = newText;
        }

        public TToken CurrentNodeType { get; }
        public TToken PreviousNodeType { get; }
        public string PreviousNodeValue { get; }

        public string CurrentNodeValue { get; }
//        public string NewText => this?.newTextFunc(default, default);
    }
}