namespace Base2art.Language.Internals
{
    public class Token<T> : IToken<T>
    {
        public T Type { get; set; }
        public string Value { get; set; }
    }
}