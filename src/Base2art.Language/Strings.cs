namespace Base2art.Language
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    public static class Strings
    {
        public static Stream AsStream(this string s)
        {
            var stream = new MemoryStream();
            using (var writer = new StreamWriter(stream, Encoding.Default, 1024, true))
            {
                writer.Write(s);
                writer.Flush();
            }

            stream.Position = 0;
            return stream;
        }

        public static void ReadLines(this string value, Action<int, string> lineHandler)
        {
            using (StringReader sr = new StringReader(value))
            {
                string line;
                int i = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    lineHandler(i, line);
                    i += 1;
                }
            }
        }

        public static void ReadLines(this string value, Action<string> lineHandler)
        {
            value.ReadLines((idx, x) => lineHandler(x));
        }

        public static string[] ReadLines(this string value)
        {
            List<string> items = new List<string>();
            value.ReadLines((x) => items.Add(x));
            return items.ToArray();
        }
    }
}