namespace Base2art.Language
{
    using System;

    public interface INode<TToken>
        where TToken : struct, Enum
    {
        TToken Type { get; }

        string Value { get; }
    }
}