namespace Base2art.Language
{
    using System.Collections.Generic;
    using System.Linq;

    public abstract class TokenContainerBase<T> : ITokenContainer<T>
    {
        private readonly List<T> items = new List<T>();

        public IEnumerable<T> Tokens()
        {
            return this.items.Concat(new T[] {this.EndOfFile()});
        }

        protected abstract T EndOfFile();

        public void Push(T node)
        {
            this.items.Add(node);
        }

        public T Pop()
        {
            var last = this.items[this.items.Count - 1];
            this.items.RemoveAt(this.items.Count - 1);
            return last;
        }

        public T Peek() => this.items[this.items.Count - 1];

        public T[] ToArray() => this.items.ToArray();
    }
}