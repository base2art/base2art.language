namespace Base2art.Language
{
    using System;
    using System.IO;

    public class StreamAdvancer
    {
        private readonly Stream stream;

        private char last;

        private char next;

        private bool hasInited;

        public StreamAdvancer(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            this.stream = stream;
            this.last = (char)(0);
            this.next = (char)(0);
        }

        public char Next()
        {
            if (!this.hasInited)
            {
                this.Advance();
                this.hasInited = true;
            }
            
            this.Advance();
            return this.Current;
        }

        public char Current
        {
            get
            {
                if (!this.hasInited)
                {
                    this.Advance();
                    this.Advance();
                    this.hasInited = true;
                }
                
                return this.last;
            }
        }

        public void Advance()
        {
            this.last = this.next;
            this.next = (char)this.stream.ReadByte();
        }
    }
}