namespace Base2art.Language
{
    using System.Collections.Generic;

    public interface ILexer<TDom>
    {
        void Lex(TDom doc);
    }

    public abstract class LexerBase<TTokenizer, TTokenType, TDom> : ILexer<TDom>
        where TTokenizer : ITokenizer<TTokenType>
    {
        private readonly TTokenizer bodyTokenizer;

        private bool hasLexed;

        public LexerBase(TTokenizer bodyTokenizer)
        {
            this.bodyTokenizer = bodyTokenizer;
        }

        public void Lex(TDom doc)
        {
            this.EnsureLexed(doc);
        }

        private void EnsureLexed(TDom doc)
        {
            if (!this.hasLexed)
            {
                this.LexInternal(doc);
                this.hasLexed = true;
            }
        }

        private void LexInternal(TDom doc)
        {
            var results = this.bodyTokenizer.Parse().GetEnumerator();

            if (results.MoveNext())
            {
                if (results.Current != null)
                {
                    this.Start(doc, results);
                }

//                this.SwallowSpace(results);
//                this.Reset(doc, doc, results);
            }
        }

        protected abstract void Start(TDom doc, IEnumerator<IToken<TTokenType>> results);

//        protected abstract void LexInternal(TDom doc);

//        private void LexInternal(TDom doc)
//        {
//            var results = this.bodyTokenizer.Parse().GetEnumerator();
//
//            if (results.MoveNext())
//            {
//                this.SwallowSpace(results);
//                this.Reset(doc, doc, results);
//            }
//        }
    }
}