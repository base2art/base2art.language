namespace Base2art.Language
{
    using System.Collections.Generic;
    using Internals;

    public interface IMacroLookup<TNode, TToken>
    {
        IEnumerable<IMacro<TToken>> BackMacros { get; }
        IEnumerable<IForwardMacro<TToken>> ForwardMacros { get; }
        
        TNode Create(IMacro<TToken> macro, TNode pop, TNode current);
        TNode Create(IForwardMacro<TToken> macro, TNode pop, TNode current, TNode next);
    }
}