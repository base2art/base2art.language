namespace Base2art.Language
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Internals;

    public class MacroProcessor<TInput, TNode, TToken>
        where TInput : ITokenContainer<TNode>, new()
        where TNode : class, INode<TToken>
        where TToken : struct, Enum
    {
        private readonly ILanguageSettings<TToken> settings;
        private readonly IMacroLookup<TNode, TToken> macros;

        public MacroProcessor(ILanguageSettings<TToken> settings, IMacroLookup<TNode, TToken> macros)
        {
            this.settings = settings;
            this.macros = macros;
        }

        public TInput Run(TInput sentence)
        {
            var words = sentence.Tokens();

            var enumer = words.GetEnumerator();
            var output = new TInput();
            if (enumer.MoveNext())
            {
                var macrosBackMacros = this.macros.BackMacros.ToList();
                var macrosForwardMacros = this.macros.ForwardMacros.ToList();
                var next = this.Word(enumer, output, null, macrosBackMacros, macrosForwardMacros);
                while (next != null)
                {
                    next = this.Word(enumer, output, next, macrosBackMacros, macrosForwardMacros);
                }
            }

            return output;
        }

        private bool StEq(string x, string y)
            => string.Equals(x, y, StringComparison.OrdinalIgnoreCase);

        private TNode Word(
            IEnumerator<TNode> sentence,
            TInput nodes,
            TNode last,
            List<IMacro<TToken>> macrosBackMacros,
            List<IForwardMacro<TToken>> macrosForwardMacros)
        {
            var current = sentence.Current;

            if (current.Type.Equals(this.settings.StopToken))
            {
                nodes.Push(current);
                return null;
            }

            
            
            foreach (var macro in macrosBackMacros)
            {
                if (macro.PreviousNodeType.Equals((last?.Type).GetValueOrDefault()) && current.Type.Equals(macro.CurrentNodeType))
                {
                    if ((macro.CurrentNodeValue == null || StEq(current.ToString(), macro.CurrentNodeValue))
                        && (macro.PreviousNodeValue == null || StEq(last.ToString(), macro.PreviousNodeValue)))
                    {
                        var node = this.macros.Create(macro, nodes.Peek(), current);
                        if (node == null)
                        {
                            nodes.Push(current);
                            sentence.MoveNext();
                            // this.Word(sentence, nodes, current, macrosBackMacros, macrosForwardMacros);
                            return current;
                        }

                        nodes.Pop();
                        nodes.Push(node);

                        sentence.MoveNext();

                        // this.Word(sentence, nodes, current, macrosBackMacros, macrosForwardMacros);
                        return current;
                    }
                }
            }

            TNode next = null;
            foreach (var macro in macrosForwardMacros)
            {
                if (macro.PreviousNodeType.Equals((last?.Type).GetValueOrDefault()) && current.Type.Equals(macro.CurrentNodeType))
                {
                    if ((macro.CurrentNodeValue == null || StEq(current.ToString(), macro.CurrentNodeValue))
                        && (macro.PreviousNodeValue == null || StEq(last.ToString(), macro.PreviousNodeValue)))
                    {

                        if (next == null)
                        {
                            sentence.MoveNext();
                            next = sentence.Current;
                        }

                        if (next.Type.Equals(macro.ForwardNodeType) &&
                            (macro.ForwardNodeValue == null || StEq(next.ToString(), macro.ForwardNodeValue)))
                        {
                            var node = this.macros.Create(macro, nodes.Peek(), current, sentence.Current);
                            if (node != null)
                            {
                                nodes.Pop();
                                nodes.Push(node);

                                sentence.MoveNext();

                                // this.Word(sentence, nodes, current, macrosBackMacros, macrosForwardMacros);
                                return current;
                            }

                            nodes.Push(current);
                            // this.Word(sentence, nodes, current, macrosBackMacros, macrosForwardMacros);
                            return current;
                        }
                    }
                }
            }

            nodes.Push(current);

            if (next == null)
            {
                sentence.MoveNext();
            }

            // this.Word(sentence, nodes, current, macrosBackMacros, macrosForwardMacros);
            return current;
        }
    }
}