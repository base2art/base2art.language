namespace Base2art.Language
{
    public interface IForwardMacro<TToken>
    {
        TToken PreviousNodeType { get; }
        string PreviousNodeValue { get; }
        TToken CurrentNodeType { get; }
        string CurrentNodeValue { get; }
        TToken ForwardNodeType { get; }
        string ForwardNodeValue { get; }
//        string NewText { get; }
    }
}