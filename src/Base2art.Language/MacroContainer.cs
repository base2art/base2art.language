namespace Base2art.Language
{
    using System;
    using System.Collections.Generic;
    using Internals;

    public class MacroContainer<TNode, TToken> : IMacroLookup<TNode, TToken>
    {
        private readonly Dictionary<IForwardMacro<TToken>, Func<TNode[], TNode>> forwardMacros =
            new Dictionary<IForwardMacro<TToken>, Func<TNode[], TNode>>();

        private readonly Dictionary<IMacro<TToken>, Func<TNode[], TNode>> backMacros = new Dictionary<IMacro<TToken>, Func<TNode[], TNode>>();

        public MacroContainer()
        {
//            this.ForwardMacros = this.forwardMacros;
//            this.BackMacros = this.backMacros;
        }

        public IEnumerable<IMacro<TToken>> BackMacros => this.backMacros.Keys;

        public IEnumerable<IForwardMacro<TToken>> ForwardMacros => this.forwardMacros.Keys;

        protected void Add(TToken previousToken, string previousText, TToken currentToken, string currentText, Func<TNode, TNode, TNode> lookup)
        {
            var macro = new Macro<TToken>(previousToken, previousText, currentToken, currentText);
            this.backMacros[macro] = nodes => lookup(nodes[0], nodes[1]);
        }

        protected void Add(
            TToken previousToken,
            string previousText,
            TToken currentToken,
            string currentText,
            TToken forwardToken,
            string forwardText,
            Func<TNode, TNode, TNode, TNode> lookup)
        {
            var macro = new ForwardMacro<TToken>(previousToken, previousText, currentToken, currentText, forwardToken, forwardText);
            this.forwardMacros[macro] = nodes => lookup(nodes[0], nodes[1], nodes[2]);
        }

        public TNode Create(IMacro<TToken> macro, TNode pop, TNode current)
            => this.backMacros.ContainsKey(macro) ? this.backMacros[macro](new[] {pop, current}) : default;

//
        public TNode Create(IForwardMacro<TToken> macro, TNode pop, TNode current, TNode next)
            => this.forwardMacros.ContainsKey(macro) ? this.forwardMacros[macro](new[] {pop, current, next}) : default;

//            => new ExpandedNode3(macro.NewText, pop, current, next);
    }
}