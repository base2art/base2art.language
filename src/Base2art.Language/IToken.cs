namespace Base2art.Language
{
    public interface IToken<TTokenType>
    {
        TTokenType Type { get; }
        
        string Value { get; }
    }
}