namespace Base2art.Language
{
    using System;
    using System.Runtime.Serialization;

    public class CompilationException : Exception
    {
        public CompilationException()
        {
        }

        protected CompilationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public CompilationException(string message) : base(message)
        {
        }

        public CompilationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}