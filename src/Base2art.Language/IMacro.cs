namespace Base2art.Language
{
    public interface IMacro<TToken>
    {
        TToken CurrentNodeType { get; }
        TToken PreviousNodeType { get; }
        string PreviousNodeValue { get; }
        string CurrentNodeValue { get; }
//        string NewText { get; }
    }
}