namespace Base2art.Language
{
    using System.Collections.Generic;

    public interface ITokenizer<T>
    {
        IEnumerable<IToken<T>> Parse();
    }
}