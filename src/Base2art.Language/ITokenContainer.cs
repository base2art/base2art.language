namespace Base2art.Language
{
    using System.Collections.Generic;

    public interface ITokenContainer<TNode>
    {
        IEnumerable<TNode> Tokens();
        void Push(TNode node);
        TNode Pop();
        TNode Peek();
    }
}