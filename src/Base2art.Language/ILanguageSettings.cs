namespace Base2art.Language
{
    using System;

    public interface ILanguageSettings<TToken>
        where TToken : Enum
    {
        TToken StopToken { get; }
    }
}