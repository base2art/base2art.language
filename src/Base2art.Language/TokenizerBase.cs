namespace Base2art.Language
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Internals;

    public abstract class TokenizerBase<T> : ITokenizer<T>
        where T : struct
    {
        private readonly StreamAdvancer stream;

        private Dictionary<char, T> singleCharTokens;
        private List<Func<char, T?>> singleCharTokenLookup;

        protected TokenizerBase(StreamAdvancer stream)
        {
            this.stream = stream;
        }

        protected abstract T EndingToken { get; }

        private (Dictionary<char, T> first, List<Func<char, T?>> fallback) CreateSingleCharTokensLookupInternal()
        {
            var first = new Dictionary<char, T>
                        {
                            {char.MaxValue, this.EndingToken},
                            {char.MinValue, this.EndingToken}
                        };

            List<Func<char, T?>> fallback = new List<Func<char, T?>>();
            this.CreateSingleCharTokensLookup(first, fallback);

            return (first, fallback);
        }

        protected virtual void CreateSingleCharTokensLookup(Dictionary<char, T> first, List<Func<char, T?>> fallback)
        {
        }

        public IEnumerable<IToken<T>> Parse()
        {
            var tokens = new LinkedList<Token<T>>();

            while (this.Token(tokens))
            {
            }

            var items = new List<IToken<T>>();

            foreach (var token in tokens)
            {
                items.Add(token);
            }

            return items;
        }

        protected bool Reset(LinkedList<Token<T>> tokens, StringBuilder current, T type)
        {
            return this.Reset(tokens, current, type, false);
        }

        protected bool Reset(LinkedList<Token<T>> tokens, StringBuilder current, T type, bool advance)
        {
            if (advance)
            {
                this.stream.Advance();
            }

            tokens.AddLast(new Token<T>
                           {
                               Type = type,
                               Value = current.ToString()
                           });

            if (type.Equals(this.EndingToken))
            {
                return false;
            }

            return true;
//            this.Token(tokens);
        }

        protected char Advance()
        {
            this.stream.Advance();
            return this.stream.Current;
        }

        protected bool Token(LinkedList<Token<T>> tokens)
        {
            //            this.stream.Advance();
            return this.HandleToken(tokens, new StringBuilder(), this.stream.Current);
        }

        protected bool HandleToken(LinkedList<Token<T>> tokens, StringBuilder sb, char c)
        {
            if (this.singleCharTokens == null)
            {
                var (first, fallback) = this.CreateSingleCharTokensLookupInternal();
                this.singleCharTokens = first;
                this.singleCharTokenLookup = fallback;
            }

            if (this.singleCharTokens.ContainsKey(c))
            {
                var singleCharToken = this.singleCharTokens[c];
                this.ProcessSingleCharToken(tokens, sb, c, singleCharToken);
                return !singleCharToken.Equals(this.EndingToken);
            }

            foreach (var lookup in this.singleCharTokenLookup)
            {
                var singleCharToken = lookup(c);
                if (singleCharToken.HasValue)
                {
                    this.ProcessSingleCharToken(tokens, sb, c, singleCharToken.Value);
                    return !singleCharToken.Value.Equals(this.EndingToken);
                }
            }

            return this.HandleMultiCharacterToken(tokens, sb, c);
        }

        protected virtual bool HandleMultiCharacterToken(LinkedList<Token<T>> tokens, StringBuilder sb, char c)
        {
            return true;
        }

        protected virtual void ProcessSingleCharToken(
            LinkedList<Token<T>> tokens,
            StringBuilder sb,
            char c,
            T type)
        {
            sb.Append(c);
            this.Reset(tokens, sb, type, true);
        }
    }
}