using Xunit;

namespace Base2art.Language.Features
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Aspose.Html;
    using Aspose.Html.Dom.Traversal;
    using Aspose.Html.Dom.Traversal.Filters;
    using Extractor;
    using Extractor.Dom;
    using Xunit.Abstractions;

    public class UnitTest1
    {
        private readonly ITestOutputHelper testOutputHelper;

        public UnitTest1(ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async void Test2()
        {
            var extractor = new DefaultNameExtractor(59);
            var values = await extractor.Search("This is a test from Tom Segura and Bert Kreischer");
            this.PrintNames(values);
            values = await extractor.Search("This is a test from Whitney Cummings and Jane Doe");
            this.PrintNames(values);
        }

        [Theory]
        [InlineData("https://en.wikipedia.org/wiki/Instant_Family")]
        public async void Test3(string url)
        {
            var content = await GetPageContent(url);
            var theContent = "";
            using (var document = new HTMLDocument(content, ""))
            {
                // The first way of gathering text elements from document
                // Initialize the instance of node iterator
                INodeIterator iterator = document.CreateNodeIterator(document, NodeFilter.SHOW_TEXT, new StyleFilter());
                StringBuilder sb = new StringBuilder();
                Aspose.Html.Dom.Node node;
                while ((node = iterator.NextNode()) != null)
                {
                    sb.Append(node.NodeValue);
                    sb.Append(' ');
                }

                theContent = sb.ToString();
            }

            var clean = theContent.ReadLines()
                                  .Where(x=> !string.IsNullOrWhiteSpace(x))
                                  .Select(x=> x.Replace("\t", " "));

            var message = string.Join(".  ", clean);
            testOutputHelper.WriteLine(message);
            var extractor = new DefaultNameExtractor(40);
            var values = await extractor.Search(message);
            this.PrintNames(values);
        }

        private static async Task<string> GetPageContent(string url)
        {
            WebRequest request = WebRequest.Create(url);
            using (var response = await request.GetResponseAsync())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        return await sr.ReadToEndAsync();
                    }
                }
            }
        }

        [Fact]
        public async void Test1()
        {
            var parser = new SentenceParser();

            var macroProcessor = new SentenceMacroProcessor();
            // IDataFactory data = new HardCodedDataFactory();
            IDataFactory data = new ResourcedDataFactory();
            var analyzer = new SemanticAnalyzer(await data.LoadData());

            var lexer = new SentenceLexer("This is a test from Tom Segura and Bert Kreischer".AsStream());
            var sentence = new Sentence();
            lexer.Lex(sentence);
            sentence = macroProcessor.Run(sentence);
            var names = analyzer.ProbableNames(parser.ProbableNames(sentence), 59);

            this.PrintNames(names.Unfold());
        }

        private void PrintNames(IEnumerable<KeyValuePair<string, AnalyzerMetaItem>> names)
        {
            foreach (var meta in names)
            {
                testOutputHelper.WriteLine($"{meta.Key} | {meta.Value.LikelyName}");
            }
        }

        class StyleFilter : Aspose.Html.Dom.Traversal.Filters.NodeFilter
        {
            public override short AcceptNode(Aspose.Html.Dom.Node n)
            {
                //If you want to avoid any element, write its name in capital letters
                return (n.ParentElement.TagName == "STYLE" || n.ParentElement.TagName == "SCRIPT" ? FILTER_REJECT : FILTER_ACCEPT);
            }
        }
    }
}