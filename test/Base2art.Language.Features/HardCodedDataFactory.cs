namespace Base2art.Language.Features
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Extractor;

    [Obsolete("Testing", true)]
    public class HardCodedDataFactory : FileBasedDataFactory
    {
        protected override async Task<IEnumerable<string>> SurnameLines()
            => await File.ReadAllLinesAsync("/home/tyoung/code/b2a/base2art.language/src/Base2art.Language/Resx/surnames.lines");

        protected override async Task<IEnumerable<string>> GivenNameLines()
            => await File.ReadAllLinesAsync("/home/tyoung/code/b2a/base2art.language/src/Base2art.Language/Resx/known-names.lines");

        protected override async Task<IEnumerable<string>> GetCommonWordsLines()
            => await File.ReadAllLinesAsync("/home/tyoung/code/b2a/base2art.language/src/Base2art.Language/Resx/words.en");
    }

    /*: IDataFactory
{
    public async Task<IDataContainer> LoadData()
    {
        return new DataContainer(
                                 CompoundNamesParts(),
                                 await Lines(),
                                 await GivenNames(),
                                 await Surnames());
    }

    public static HashSet<string> CompoundNamesParts()
    {
        return new HashSet<string>
               {
                   "los",
                   "le",
                   "al",
                   "do",
                   "du",
                   "den",
                   "del",
                   "dos",
                   "la",
                   "bin",
                   "van",
                   "von",
                   "der",
                   "de",
                   "ben",
                   "bint",

                   "da",
                   "dela",
                   "el"
               };
    }

    public static async Task<Dictionary<string, int>> Lines()
    {
        var lines = await File.ReadAllLinesAsync("/home/tyoung/code/b2a/base2art.language/src/Base2art.Language/Resx/words.en");
//        HashSet<string> words = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        Dictionary<string, int> words = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

        foreach (var line in lines.Select(x => x.Trim()).Where(x => !String.IsNullOrWhiteSpace(x)))
        {
            var parts = line.Split(':');
            if (parts.Length == 2)
            {
                words.Add(parts[0], Int32.Parse(parts[1]));
            }
        }

        return words;
    }

    public static async Task<Dictionary<string, int>> GivenNames()
    {
        var lines = await File.ReadAllLinesAsync("/home/tyoung/code/b2a/base2art.language/src/Base2art.Language/Resx/known-names.lines");
        Dictionary<string, int> words = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

        foreach (var line in lines.Select(x => x.Trim()).Where(x => !String.IsNullOrWhiteSpace(x)))
        {
            var parts = line.Split(':');
            if (parts.Length == 2)
            {
                words.Add(parts[0], Int32.Parse(parts[1]));
            }
        }

        return words;
    }

    public static async Task<Dictionary<string, int>> Surnames()
    {
        var lines = await File.ReadAllLinesAsync("/home/tyoung/code/b2a/base2art.language/src/Base2art.Language/Resx/surnames.lines");
        Dictionary<string, int> words = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

        foreach (var line in lines.Select(x => x.Trim()).Where(x => !String.IsNullOrWhiteSpace(x)))
        {
            var parts = line.Split(',');
            if (parts.Length == 15)
            {
                var value = Int32.Parse(parts[1]);

                var cost = 1;
                if (value < 1666)
                {
                    cost = 1;
                }
                else if (value < 3333)
                {
                    cost = 2;
                }
                else
                {
                    cost = 3;
                }

                words.Add(parts[0], cost);
            }
        }

        return words;
    }

    private class DataContainer : IDataContainer
    {
        private readonly HashSet<string> compoundNamesParts;
        private readonly IReadOnlyDictionary<string, int> words;
        private readonly IReadOnlyDictionary<string, int> givenNames;
        private readonly IReadOnlyDictionary<string, int> surnames;

        public DataContainer(
            HashSet<string> compoundNamesParts,
            IReadOnlyDictionary<string, int> words,
            IReadOnlyDictionary<string, int> givenNames,
            IReadOnlyDictionary<string, int> surnames)
        {
            this.compoundNamesParts = compoundNamesParts;
            this.words = words;
            this.givenNames = givenNames;
            this.surnames = surnames;
        }

        public HashSet<string> CompoundNamesParts() => this.compoundNamesParts;

        public IReadOnlyDictionary<string, int> Words() => this.words;

        public IReadOnlyDictionary<string, int> GivenNames() => this.givenNames;

        public IReadOnlyDictionary<string, int> Surnames() => this.surnames;
    }
}*/
}